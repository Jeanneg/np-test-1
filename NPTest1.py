# -*- coding: utf-8 -*-

import sys, getopt
import requests
import json

# Set default values
inputfile = 'https://api.npolar.no/marine/biology/sample/?q=&fields=expedition,utc_date,programs,conveyance&limit=all&format=json&variant=array'
outputfile = 'data.json'

# Get stdin inputs
argv = sys.argv[1:]
try:
    opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
except getopt.GetoptError:
        print('NPtest1.py -i <"data input link> -o <outputfile">')
        sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print('NPtest1.py -i <"data input link"> -o <outputfile>')
        sys.exit()
    elif opt in ("-i", "--ifile"):
        inputfile = arg
    elif opt in ("-o", "--ofile"):
        outputfile = arg

# read and parse data
req = requests.get(inputfile)
data  = json.loads(req.text)

# Select the first date of each expedition
tmp = data
tmp.sort(key=lambda x: x.get('utc_date'), reverse=True)
tmp = list({v['expedition']:v for v in data}.values())
# Select the last date of each expedition
data.sort(key=lambda x: x.get('utc_date'))
data = list({v['expedition']:v for v in data}.values())
# Add the first date to the list
for n in range(len(data)):
    for k in range(len(data)):
        if data[n].get('expedition') == tmp[k].get('expedition'):
            data[n].update(first_sample=tmp[k].get('utc_date'))
            data[n]['last_sample'] = data[n].pop('utc_date')
            break
# Reorder in first sampling chronological orger
data.sort(key=lambda x: x.get('first_sample'))  
# Write to file
with open(outputfile, 'w') as fp:
    fp.write(
            '[' +
            ',\n'.join(json.dumps(n) for n in data) +
            ']\n')
fp.close()
